import { ACTIONS } from 'constants/index';
import fetch from 'utils/fetch';
import { SERVICES } from 'configs';

export function fenchPostListByUser(id) {
  return dispatch => {
    const options = {
      method: 'get',
      url: `${SERVICES.GET_POST_LIST_BY_USER}/${id}/posts`,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(rest => {
        dispatch(listOfUserFetchedAction(rest));
        dispatch(doneLoadingAction());
      })
      .catch(() => {
        dispatch(listOfUserFetchedAction({}));
        dispatch(doneLoadingAction());
      });
  };
}

export function fenchDeletePostById(data) {
  return dispatch => {
    const options = {
      method: 'delete',
      url: `${SERVICES.GET_POST_LIST_BY_USER}/${data.userId}/posts`,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      },
      data: { postId: data.postId }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(rest => {
        dispatch(deleteOfUserFetchedAction(rest));
        dispatch(fenchPostListByUser(data.userId));
        dispatch(doneLoadingAction());
      })
      .catch(() => {
        dispatch(deleteOfUserFetchedAction({}));
        dispatch(doneLoadingAction());
      });
  };
}


function loadingAction() {
  return { type: ACTIONS.LOADING };
}

function listOfUserFetchedAction(data) {
  return {
    type: ACTIONS.LIST_OF_POST_BY_USER_FETCHED,
    data
  };
}

function deleteOfUserFetchedAction(data) {
  return {
    type: ACTIONS.LIST_OF_POST_BY_USER_FETCHED,
    data
  };
}

function doneLoadingAction() {
  return { type: ACTIONS.DONE_LOADING };
}