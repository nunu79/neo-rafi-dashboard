import React from "react";
import PropTypes from 'prop-types';
import Grid from "@material-ui/core/Grid";
import GridItem from "components/Grid/GridItem.jsx";
import Table from "components/Table/Table.jsx";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableFooter from "@material-ui/core/TableFooter";
import IconButton from "@material-ui/core/IconButton";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import Edit from "@material-ui/icons/Edit";
//import { ROUTES } from 'configs';
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Tooltip from "@material-ui/core/Tooltip";
import UltimatePaging from 'components/forms/UltimatePaging';
import moment from 'moment';
import Modal from 'components/Modal/Modal.jsx';
import { ACTIONS } from 'constants/index';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      id: null,
      rid: null,
      title: '',
      coupon: '',
      page: 1,
      total: 3,
      render: false
    };
    this._closeModal = this._closeModal.bind(this);
    this._handleEditRedeem = this._handleEditRedeem.bind(this);
    this.onPageChange = this.onPageChange.bind(this);
  }

  componentWillMount() {
    const { actions, page } = this.props;

    actions.fetchRedeemRewardList({ page: (page === undefined ? 1 : page), count: 10 });
  }

  componentWillReceiveProps(nextProps) {
    let { LIST_OF_REDEEM_REWARD_FETCHED } = ACTIONS;
    let { type } = nextProps;

    if( type === LIST_OF_REDEEM_REWARD_FETCHED ) {
      this.setState({render: true});
    }
  }

  _handleEditRedeem() {
    const {
      actions
    } = this.props;

    let { id, rid, coupon } = this.state;
    let val = { userId: id, rewardId: rid, coupon };
    actions.fetchRedeemEdit(val);

    this.setState({
      open: false,
      id: null,
      rd: null,
      title: ''
    });
  }

  onPageChange(page) {
    const { actions } = this.props;
    const { meta } = this.props.data;

    this.setState({ page });
    actions.fetchRedeemRewardList({ page: page, count: meta.size });
  }

  _handleChange(coupon) {
    this.setState({ coupon });
  }

  _openModal(id, rid, title) {
    this.setState({
      open: true,
      id,
      rid,
      title
    });
  }

  _closeModal() {
    this.setState({
      open: false,
      id: null,
      rd: null,
      title: ''
    });
  }

  _renderTable() {
    const { data } = this.props;
    if(!this.state.render) {
      return (
        <h1>TIDAK ADA DATA</h1>
      );
    } else {
      return (
        <Table
          tableHeaderColor="primary"
          tableHead={["No", "Nama Customer", "E-mail", "Phone", "Reward Name", "Date", "Coupon", "Actions"]}
        >
          <TableBody>
          {data && data.data &&
            (
              data.data.map((item, index) => this._renderTableRow(item, index + 1))
            )
          }
          </TableBody>
          {this._renderTableMeta()}
        </Table>
      );
    }
  }

  _renderTableRow(redeem, index) {
    const { userId, username, email, phone, rewardName, 
      rewardRedeemCreatedAt,rewardCoupon, _id, rewardId } = redeem;
    const { classes, data } = this.props;
    let { page, size } = data.meta;

    return (
      <TableRow key={_id}>
        <TableCell className={classes.tableCell}>{((page-1)* size)+index}</TableCell>
        <TableCell className={classes.tableCell}>{username}</TableCell>
        <TableCell className={classes.tableCell}>{email}</TableCell>
        <TableCell className={classes.tableCell}>{phone}</TableCell>
        <TableCell className={classes.tableCell}>{rewardName}</TableCell>
        <TableCell className={classes.tableCell}>{moment(rewardRedeemCreatedAt).format('DD-MM-YYYY')}</TableCell>
        <TableCell className={classes.tableCell}>{rewardCoupon}</TableCell>
        <TableCell className={classes.tableCell}>
          <Tooltip
            id="tooltip-top"
            title="Redeem Edit"
            placement="top"
            classes={{ tooltip: classes.tooltip }}
          >
            <IconButton
              onClick={() => this._openModal(userId, rewardId, rewardName)}
              aria-label="Close"
              className={classes.tableActionButton}
            >
              <Edit
                className={
                  classes.tableActionButtonIcon + " " + classes.edit
                }
              />
            </IconButton>
          </Tooltip>
        </TableCell>
      </TableRow>
    );
  }

  _renderTableMeta() {
    const { data, page } = this.props;

    if (data && data.meta) {
      return (
        <TableFooter>
          <TableRow>
            <TableCell colSpan={4}>
              <UltimatePaging
                totalPages={(data.meta.totalPage !== undefined) 
                  ? data.meta.totalPage : 1}
                currentPage={page}
                onChange={this.onPageChange}
                />
            </TableCell>
          </TableRow>
        </TableFooter>
      );
    } else {
      return (<TableFooter />);
    }
  }

  _renderModal() {
    let { classes } = this.props;
    return (
      <Modal open={this.state.open} closeModal={this._closeModal} onClick={this._handleEditRedeem}>
        <div>
          <h6 className={classes.modalTitle}>
            {
              'Edit redeem:' + this.state.title
            }
          </h6>
          <CustomInput
            labelText="coupon"
            id="coupon"
            formControlProps={{
              fullWidth: true
            }}
            onChange = {
              (txt) => {
                this._handleChange(txt.target.value);
              }
            }
          />

        </div>
      </Modal>
    );
  }

  render() {
    const { isLoading, classes } = this.props;

    return (
      <Grid container>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>Redeem Reward</h4>
              <p className={classes.cardCategoryWhite}>
                 
              </p>
            </CardHeader>
            <CardBody>
              {isLoading ? <h6>Loading..</h6> : this._renderTable()}
            </CardBody>
          </Card>
        </GridItem>
        {this._renderModal()}
      </Grid>
    );
  }

}
Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
  data: PropTypes.object,
  isLoading: PropTypes.bool,
  page: PropTypes.number,
  count: PropTypes.number
};