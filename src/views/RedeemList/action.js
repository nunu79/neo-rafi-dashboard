import { ACTIONS } from 'constants/index';
import fetch from 'utils/fetch';
import { SERVICES } from 'configs';

export function fetchRewardList(data) {
  return dispatch => {
    const options = {
      method: 'get',
      url: SERVICES.GET_REDEEM_REWARD_LIST+ '?page=' + data.page + '&size=' + data.count,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(res => {
        dispatch(listORewardFetchedAction(res, data.page, data.count));
        dispatch(doneLoadingAction());
      })
      .catch(() => {
        dispatch(listORewardFetchedAction([], data.page, data.count));
        dispatch(doneLoadingAction());
      });
  };
}

export function fetchRedeemEdit(data) {
  return dispatch => {
    const options = {
      method: 'post',
      url: SERVICES.EDIT_REDEEM,
      data,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(res => {
        dispatch(editOfRedeemFetchedAction(res));
        dispatch(doneLoadingAction());
        window.location.href = '/redeem';
      })
      .catch(() => {
        dispatch(editOfRedeemFetchedAction([]));
        dispatch(doneLoadingAction());
      });
  };
}

export function fetchRedeemRewardList(data) {
  return dispatch => {
    const options = {
      method: 'get',
      url: SERVICES.GET_REDEEM_REWARD_LIST + '?page=' + data.page + '&size=' + data.count,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(res => {
        dispatch(listORedeemRewardFetchedAction(res, data.page, data.count));
        dispatch(doneLoadingAction());
      })
      .catch(() => {
        dispatch(listORedeemRewardFetchedAction([], data.page, data.count));
        dispatch(doneLoadingAction());
      });
  };
}


function loadingAction() {
  return { type: ACTIONS.LOADING };
}

function listORewardFetchedAction(data, page, count) {
  return {
    type: ACTIONS.LIST_OF_REWARD_FETCHED,
    data,
    page,
    count
  };
}

function listORedeemRewardFetchedAction(data, page, count) {
  return {
    type: ACTIONS.LIST_OF_REDEEM_REWARD_FETCHED,
    data,
    page,
    count
  };
}

function editOfRedeemFetchedAction(data) {
  return {
    type: ACTIONS.EDIT_REDEEM,
    data
  };
}

function doneLoadingAction() {
  return { type: ACTIONS.DONE_LOADING };
}