import { ACTIONS } from 'constants/index';

const initialState = {
  data:{}
};

export default function reducer(state = initialState, action) {
  const { DETAIL_OF_PROMO_FETCHED } = ACTIONS;
  const { type, data } = action;

  switch (type) {
    case DETAIL_OF_PROMO_FETCHED:
      return {
        ...state,
        isLoading: false,
        data,
        type
      };
    default:
      return state;
  }
}
