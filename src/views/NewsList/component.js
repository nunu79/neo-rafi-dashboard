import React from 'react';
import PropTypes from "prop-types";
import TravelNews from 'containers/TravelNews';
import CulinaryNews from 'containers/CulinaryNews';
//import MudikInfo from 'containers/MudikInfo';
//import MudikRoute from 'containers/MudikRoute';
import CustomTabs from "components/CustomTabs/CustomTabs.jsx";

export default class Component extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      id: null,
      title: '',
    };
    this._closeModal = this._closeModal.bind(this);
    this._handleDeleteNews = this._handleDeleteNews.bind(this);
  }

  _handleDeleteNews = () => {
    const { actions } = this.props;
    //    actions.fetchDeleteUser(userId);
    actions.fetchNewsDelete(this.state.id);
    this.setState({ open: false, id:null, title:'' });
  }

  _openModal(id, title) {
    this.setState({ open: true, id, title });
  }

  _closeModal() {
    this.setState({ open: false, id:null, title:'' });
  }

  render() {

    return (
      <CustomTabs
        plainTabs={true}
        headerColor="primary"
        tabs={[
          {
            tabName: "Wisata",
            tabContent: (
              <TravelNews />
            )
          },
          {
            tabName: "Kuliner",
            tabContent: (
              <CulinaryNews />
            )
          },
          /*{
            tabName: "Info Mudik",
            tabContent: (
              <MudikInfo />
            )
          },
          {
            tabName: "Jalur Mudik",
            tabContent: (
              <MudikRoute />
            )
          }*/
        ]}
      />
    );
  }

}

Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
};