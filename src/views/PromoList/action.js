import { ACTIONS } from 'constants/index';
import fetch from 'utils/fetch';
import { SERVICES } from 'configs';

export function fetchPromoList() {
  return dispatch => {
    const options = {
      method: 'get',
      url: SERVICES.GET_PROMO_LIST,
    };

    dispatch(loadingAction());

    fetch(options)
      .then(res => {
        dispatch(listOfPromoFetchedAction(res));
        dispatch(doneLoadingAction());
      })
      .catch(() => {
        dispatch(listOfPromoFetchedAction([]));
        dispatch(doneLoadingAction());
      });
  };
}

export function fetchPromoDelete(data) {
  return dispatch => {
    const options = {
      method: 'delete',
      url: SERVICES.DELETE_PROMO + '/' + data,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(res => {
        dispatch(deleteOfPromoFetchedAction(res));
        dispatch(doneLoadingAction());
        window.location.href = '/promo';
      })
      .catch(() => {
        dispatch(deleteOfPromoFetchedAction([]));
        dispatch(doneLoadingAction());
      });
  };
}

function loadingAction() {
  return { type: ACTIONS.LOADING };
}

function listOfPromoFetchedAction(data) {
  return {
    type: ACTIONS.LIST_OF_PROMO_FETCHED,
    data
  };
}

function deleteOfPromoFetchedAction(data) {
  return {
    type: ACTIONS.DELETE_PROMO,
    data
  };
}

function doneLoadingAction() {
  return { type: ACTIONS.DONE_LOADING };
}