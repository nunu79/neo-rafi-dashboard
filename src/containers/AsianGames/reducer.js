import { ACTIONS } from 'constants/index';

const initialState = {
  data: []
};

export default function reducer(state = initialState, action) {
  const { LIST_OF_ASIAN_GAMES_NEWS_FETCHED } = ACTIONS;
  const { type, data } = action;

  switch (type) {
    case LIST_OF_ASIAN_GAMES_NEWS_FETCHED:
      return {
        ...state,
        isLoading: false,
        data,
        type
      };
    default:
      return state;
  }
}
