import React from "react";
import { Route, Switch } from "react-router-dom";

import indexRoutes from "routes/index.jsx";
import { getToken } from 'utils/common';
import { ROUTES } from './configs';
import views from './views'

export default class App extends React.Component {

  constructor(props) {
    super(props);
    let [isLoggedIn, shouldRender] = this._checkAuth();

    this.state = {
      isLoggedIn,
      shouldRender
    };
  }

  _checkAuth() {
    let { pathname } = window.location;
    let { HOME, LOGIN } = ROUTES;
    let accessToken = getToken();
    let isLoggedIn = (pathname === LOGIN());
    let shouldRender = true;

    if (!accessToken && pathname !== LOGIN()) {
      window.location.href = LOGIN();
      shouldRender = false;
    } else if (accessToken && pathname === LOGIN()) {
      window.location.href = HOME();
      shouldRender = false;
    }

    return [isLoggedIn, shouldRender];
  }

  render() {
    let { isLoggedIn, shouldRender } = this.state;
    let { LoginPage } = views;

    if (!shouldRender) {
      return (null);
    }

    return (
      isLoggedIn ?
        <LoginPage />
      :
        <Switch>
          {indexRoutes.map((prop, key) => {
            return <Route path={prop.path} component={prop.component} key={key} />;
          })}
        </Switch>
    );
  }
}